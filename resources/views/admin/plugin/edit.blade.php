@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Manage Plugin</div>
                    <div class="card-body col px-md-5">
                        <form action="{{ route('admin.plugin.update',['plugin'=>$plugin->id])}}" method="POST">
                            @csrf
                            {{method_field('PUT')}}
                            <label>Plugin Name</label>
                            <input type="text" name="plugin_name" value="{{$plugin->plugin_name}}">
                            <label>Plugin Version</label>
                            <input type="text" name="plugin_version" value="{{$plugin->plugin_version}}">
                            <button type="submit" class="btn btn-primary float-right">
                                Update
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
