@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Create a Plugin</div>
                    <div class="card-body col px-md-5 bs-linebreak">
                        <form action="{{ route('admin.plugin.store')}}" method="POST">
                            @csrf
                            {{method_field('POST')}}
                            <table>
                                <tr>
                                    <th>
                                        <label>UserName</label>
                                    </th>

                                    <th>
                                        <select name="user_id">
                                            <option selected disabled value="-1">Select a user</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->email}}</option>
                                            @endforeach
                                        </select>
                                    </th>

                                </tr>
                                <tr>

                                    <th>
                                        <label>Plugin Name</label>
                                    </th>
                                    <th>
                                        <input type="text" name="plugin_name">
                                    </th>

                                </tr>

                                <tr>

                                    <th>
                                        <label>Plugin Version</label>
                                    </th>
                                    <th>
                                        <input type="text" name="plugin_version">
                                    </th>

                                </tr>
                            </table>

                            <button type="submit" class="btn btn-primary float-right">
                                Create
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
