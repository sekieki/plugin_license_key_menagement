@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Manage Plugins
                        <a href="{{route('admin.plugin.create')}}" class="float-right">
                            <button type="button" class="btn btn-primary btn-sm">Create</button>
                        </a>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th scope="col">Plugin Name</th>
                                <th scope="col">Plugin Version</th>
                                <th scope="col">License Key</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($plugins as $plugin)
                                <tr>
                                    <th>{{$plugin->email}}</th>
                                    <th>{{$plugin->plugin_name}}</th>
                                    <th>{{$plugin->plugin_version}}</th>
                                    <th>
                                        <input type="text" id="copy_{{ $plugin->id }}" value="{{ $plugin->license_key }}">
                                        <button class='btn btn-primary' value="copy" onclick="copyToClipboard('copy_{{ $plugin->id }}')">Copy LicenseKey</button>
                                    </th>
                                    <th>

                                        <a href="{{route('admin.plugin.edit',$plugin->id)}}" class="float-left">
                                            <button type="button" class="btn btn-primary btn-sm ml-2">Edit</button>
                                        </a>
                                        <form action="{{route('admin.plugin.destroy',$plugin->id)}}" method="POST"
                                              class="float-left">
                                            {{method_field('DELETE')}}
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-sm ml-2">Delete</button>
                                        </form>
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    function copyToClipboard(id) {
        document.getElementById(id).select();
        document.execCommand('copy');
        alert('Copied the License Key');
    }
</script>
