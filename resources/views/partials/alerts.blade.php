@if (session ('succes'))

    <div class="alert alert-success" role="alert">

        {{ session ('succes') }}

    </div>

@endif

@if (session ('warning'))

    <div class="alert alert-warning" role="alert">

        {{ session ('warning') }}

    </div>

@endif
