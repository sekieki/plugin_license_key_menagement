@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Your Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table">
                            <thead>
                            <th>Plugin Name</th>
                            <th>Version</th>
                            <th>License Key</th>
                            </thead>
                            <tbody>
                            @foreach($plugins as $plugin)
                                <tr>
                                    <th>{{$plugin->plugin_name}}</th>
                                    <th>{{$plugin->plugin_version}}</th>
                                    <th>
                                        <input type="text" id="copy_{{ $plugin->id }}" value="{{ $plugin->license_key }}" readonly>
                                        <button class='btn btn-primary' value="copy" onclick="copyToClipboard('copy_{{ $plugin->id }}')">Copy LicenseKey</button>
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
<script>
    function copyToClipboard(id) {
        document.getElementById(id).select();
        document.execCommand('copy');
        alert('Copied the License Key');
    }
</script>
