<?php

namespace App\Http\Controllers;

use App\Plugin;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PluginController;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     *
     */

    public function index()
    {
        $plugins = Plugin::where('user_id', Auth::user()->id)->get();
        return view('home',['plugins'=>$plugins]);
    }


}
