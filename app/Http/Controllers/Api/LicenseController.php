<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Plugin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class LicenseController extends Controller
{
    public function checkLicense(Request $request){
        if (empty($request->get("license_key"))){
            return response()->json([
                'success'=>false
            ]);
        }

        if (empty($request->get("plugin_idf"))){
            return response()->json([
                'success'=>false
            ]);
        }

        try {
            $decrypted = Crypt::decrypt($request->get("license_key"));
            $data = explode(";", $decrypted);


            if (count($data) != 3 ) {
                return response()->json([
                    'success'=>false
                ]);
            }

            $pluginId = $data[0];

            if ($pluginId != $request->get("plugin_idf")){
                return response()->json([
                    'success'=>false
                ]);
            }

            $plugin = Plugin::where('id', $pluginId)
                ->where('license_key', $request->get("license_key"))
                ->first();

            if (empty($plugin)) {
                return response()->json([
                    'success'=>false
                ]);
            }

            return response()->json([
                'success'=>true
            ]);
        }catch (\Exception $e){
            return response()->json([
                'success'=>false
            ]);
        }
    }
}
