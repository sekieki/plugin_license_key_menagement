<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Plugin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;


class PluginController extends Controller
{
    public function index()
    {
        $plugins = DB::table('plugin')
            ->join('users', 'users.id', '=', 'plugin.user_id')
            ->select('plugin.*', 'users.email')
            ->get();
        return view('admin.plugin.index')->with('plugins', $plugins);

    }

    public function create()
    {
        $users = User::where('email_verified_at', '!=', null)
            ->select('id', 'email')
            ->get();
        return view('admin.plugin.create', [
            'users' => $users
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     */


    public function edit($id)
    {
        return view('admin.plugin.edit')->with(['plugin' => Plugin::find($id)]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     */
    public function destroy($id)
    {
        $plugin = Plugin::find($id);
        if ($plugin) {
            $plugin->delete();
            return redirect()->route('admin.plugin.index')->with('succes', 'Plugin has been deleted');
        }
    }


    public function update(Request $request, $id)
    {
        $plugin_name = $request->input('plugin_name');
        $plugin_version = $request->input('plugin_version');
        DB::update('update plugin set plugin_name = ?,plugin_version=? where id = ?', [$plugin_name, $plugin_version, $id]);
        return redirect()->route('admin.plugin.index', ['plugin' => Plugin::find($id)])->with('succes', 'Changes has been made.');
    }


    public function store(Request $req)
    {
        $user_id = $req->input('user_id');
        $plugin_name = $req->input('plugin_name');
        $plugin_version = $req->input('plugin_version');

        $data = [
            'user_id' => $user_id,
            'plugin_name' => $plugin_name,
            'plugin_version' => $plugin_version
        ];

        $id = DB::table('plugin')->insertGetId($data);

        $licenseData = $id . ";" . $user_id . ";" . $plugin_version;
        $licenseKey = Crypt::encrypt($licenseData);

        Plugin::where('id', $id)->update([
            'license_key' => $licenseKey
        ]);

        return redirect()->route('admin.plugin.index')->with('succes', 'Plugin has been created.');


    }

    function check()
    {
            echo 'True';
    }

}
