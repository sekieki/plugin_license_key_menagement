<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Plugin extends Model
{
    protected $table = "plugin";

    protected $fillable = ["name","user_id", "plugin_name", "plugin_version", "license_key"];

    public $timestamps = ["created_at", "updated_at"];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
